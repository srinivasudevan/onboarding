# Environment Tools

Some of the tools required for the project

## Visual Studio

Currently you need to have installed VS 2015 (community edition) or higher. If you got access to the professional or enterprise edition with a licence. Use that. Currently one MSDN licence is available for Gofundraise and that is assigned to Srini.

## VSCode

Download VS Code and set that up. It makes it easier to work with node projects, js projects and quick and easy view of source files. Download various extensions from the tool aswell to help you set up


## Libraries and Packages

Some of the libraries and packages that you may need to download for development

- Node
- Python
- dotnet core (SDK & Runtime)

## SQL Server

Download and install SQL server development edition plus sql server management studio.

Make sure "SQL Server and Windows Authentication mode" is turned on when you try to connect to your database engine with SQL authentication. 
- Right click on server > Properties 
- Go to Security tab
- Under Server authentication choose the SQL Server and Windows Authentication mode radio button.
- Restart SQL Services

## MySQL

There are projects using MySQL. You will need to download and install MySQL and the workbench.

## Docker

There are various projects that are containerised using Docker. You will need to set that up on your machine.

## Resharper

If possible, get yourself a resharper licence. It will make it so much easier to develop in C# and make refactoring an ease. Cuurently one licence is available that is assigned to Srini. 

## Builds

You will need to get setup with `Bamboo`

## Source control

You will need to get setup with `BitBucket`

GitExtensions is a really nice tool which comes with Git for windows aswell. Use that and familairise yoruself around git and its concepts. 

## Diff tool

Download a diff tool of choice so that you can integrate it with source control. You can download WinMerge for free. If you prefer a paid version, look at BeyondCompare. That is very extensive and has a lot of tooling around doing three way merges (useful for git merge conflicts).

## Task Tracking

You will need to get setup with `JIRA`